﻿#region License
/*
 * Microsoft Public License (Ms-PL)
 * 
 * This license governs use of the accompanying software. If you use the software, you accept this license. 
 * If you do not accept the license, do not use the software.
 *
 * 1. Definitions
 *
 * The terms "reproduce," "reproduction," "derivative works," and "distribution" have the same meaning here as under U.S. copyright law.
 *
 * A "contribution" is the original software, or any additions or changes to the software.
 *
 * A "contributor" is any person that distributes its contribution under this license.
 *
 * "Licensed patents" are a contributor's patent claims that read directly on its contribution.
 *
 * 2. Grant of Rights
 *
 * (A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, 
 * prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
 *
 * (B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, 
 * use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the 
 * contribution in the software.
 *
 * 3. Conditions and Limitations
 *
 * (A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
 *
 * (B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, 
 * your patent license from such contributor to the software ends automatically.
 *
 * (C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, 
 * and attribution notices that are present in the software.
 *
 * (D) If you distribute any portion of the software in source code form, you may do so only under this license by including 
 * a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, 
 * you may only do so under a license that complies with this license.
 *
 * (E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. 
 * You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, 
 * the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement. 
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Net.Sockets;
using System.Threading;
using SpankyProjectClient.Objects;
using System.Collections;
using System.Windows.Forms;

namespace SpankyProjectClient.TCP
{
    public static class Parser
    {
        static ServerObject serverObject = new ServerObject();
        /// <summary>
        /// Parses an unencrypted packet
        /// </summary>
        /// <param name="client">The client that sent the packet</param>
        /// <param name="pack">the packet</param>
        /// <param name="num_bytes">the length of the data in the packet</param>
        public static void Parse(NetworkStream client,byte[] pack, int num_bytes)
        {
            // Reformat this pak
            byte[] prepared = Parser.PreparePacket(pack, num_bytes);
            byte[] id = new byte[2];
            Array.Copy(prepared, id, 2);
            List<byte> trim = prepared.ToList<byte>();
            trim.RemoveAt(0);
            trim.RemoveAt(0);
            byte[] packet = trim.ToArray();
            // WORK WITH PACKET
            int pkt_id = int.Parse(Encoding.ASCII.GetString(id));
            switch (pkt_id)
            {
                case 12:
                    {
                        // Login request packet.
                        //byte[] unEncrypted = EncDec.Decrypt(prepared,Parser.serverObject.EncryptionKey,Parser.serverObject.IVKey);
                        string request = Encoding.ASCII.GetString(packet);
                        Spanky.WriteLine(request, Color.RosyBrown);
                        // pop up a login box
                        SpankyLogin login = new SpankyLogin();
                        DialogResult result = login.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            // create the packet 
                            byte[] byteLogin = Parser.CreateLoginPacket(login.txt_Username.Text, login.txt_Password.Text);
                            //byte[] enCrypted = EncDec.Encrypt(byteLogin, Parser.serverObject.EncryptionKey, Parser.serverObject.IVKey);
                            client.Write(byteLogin, 0, byteLogin.Length);
                        }

                    }
                    break;
                case 13: // Bad Login
                    {
                        Spanky.WriteLine("Login Denied.", Color.Blue);
                        /*
                         * Ask user if he wants to retry login or be directed to the website to create an account.
                         * build a whats our current website request packet.
                         */
                    }
                    break;
                case 14: // Good Login
                    {
                        Spanky.WriteLine("Login Accepted.", Color.Blue);

                    }
                    break;
                default:
                    Spanky.WriteLine("Unknown Packet #" + pkt_id.ToString(), Color.Red);
                    break;
            }

        }

        /// <summary>
        /// Removes the first 2 bytes from a byte array
        /// for removing the packet id from my packets.
        /// </summary>
        /// <param name="pak">the packet in bytes</param>
        /// <returns>the corrected packet minus packet id</returns>
        private static byte[] RemovePID(byte[] pak)
        {
            List<byte> tmp = new List<byte>();
            try
            {
                // First 2 bytes are always the pid
                
                tmp.AddRange(pak);
                tmp.RemoveAt(0);
                tmp.RemoveAt(0);
                
            }
            catch (Exception e)
            {
                Spanky.WriteLine(e.ToString(), Color.Red);
            }
            return tmp.ToArray();
        }
     

        /// <summary>
        /// Grabs the hello paclet from our server, this packet
        /// contains the keys we use to encrypt with from now on.
        /// </summary>
        /// <param name="pak">The packet</param>
        /// <param name="num_bytes">the length of the data</param>
        /// <returns>The server object with encryption keys</returns>
        public static ServerObject InitServerConnection(NetworkStream ns, byte[] pak, int num_bytes)
        {
            ServerObject t = new ServerObject();
            // This is the first packet we get, it contains the keys we use from now on.
            try
            {
                // Hardcoded password
                string pass = "#UgMugaBugStug483#";
                // Trim all bytes that are not part of our data
                byte[] trimmed = new byte[num_bytes];
                Array.Copy(pak, trimmed, num_bytes);
                // First packet is encrypted with the above password
                byte[] unEncrypted = EncDec.Decrypt(trimmed, pass);
                string result = Encoding.ASCII.GetString(unEncrypted);
                // So lets split this string, first 2 chars is the number 10, remove it
                string r2 = result.TrimStart('1').TrimStart('0');
                // Split the string 
                t.IVKey = Encoding.ASCII.GetBytes(r2.Split(':').Last());
                t.EncryptionKey = Encoding.ASCII.GetBytes(r2.Split(':').First());
                // Set our server
                Parser.serverObject = t;
                // tell Server we got the keys and are ready to talk.
                byte[] done = Parser.CreatePacket(11);
                ns.Write(done, 0, done.Length);
                ns.Flush();

            }
            catch (Exception e)
            {
                Spanky.WriteLine(e.ToString(), Color.Red);
            }
            return t;
        }
        /// <summary>
        /// Decrypts an incomming packet
        /// </summary>
        /// <param name="pak">The packet to decrypt</param>
        /// <param name="num_bytes">the length of the data</param>
        /// <returns></returns>
        public static byte[] PreparePacket(byte[] pak, int num_bytes)
        {
            byte[] prepared = { };
            // Copy all data, leave the rest out
            byte[] trim = new byte[num_bytes];
            Array.Copy(pak, trim, num_bytes);
            // Unencrypt it
            if (Parser.serverObject.EncryptionKey.Length == 0)
            {
                // Cant parse this packet, 
                Spanky.WriteLine("Unable to parse packet, no keys with values.", Color.Red);
                return null;
            }
            else
            {
                try
                {
                    prepared = EncDec.Decrypt(trim, Parser.serverObject.EncryptionKey, Parser.serverObject.IVKey);
                }
                catch (Exception e)
                {
                    Spanky.WriteLine(e.ToString(), Color.Red);
                }
            }
            return prepared;
        }
        /// <summary>
        /// Creates different packets to send to server
        /// depnding on the packet number we want
        /// each packet number is for a specfic function.
        /// </summary>
        /// <param name="num">The packet to make</param>
        /// <returns></returns>
        public static byte[] CreatePacket(int num)
        {
            byte[] packet = { };
            // So we have our keys now, let use them
            if (Parser.serverObject.EncryptionKey.Length == 0)
            {
                // Cant parse this packet, 
                Spanky.WriteLine("Unable to parse packet, no keys with values.", Color.Red);
                return null;
            }
            else
            {
                switch (num)
                {
                    case 11:
                        {
                            // This is the first packet we send with our newly aquired keys, its just to let the server
                            // know we have them, and are ready for the next step.
                            string keysReceived = "11KEYSRECEIVED";
                            
                            byte[] pass1 = Encoding.ASCII.GetBytes(keysReceived);
                            packet = EncDec.Encrypt(pass1, Parser.serverObject.EncryptionKey, Parser.serverObject.IVKey);

                        }
                        break;
                    case 12:
                        {
                        }
                        break;
                    default:
                        Spanky.WriteLine("Packet Creation ID ("+num+") Unknown.", Color.Red);
                        break;
                }
            }
            return packet;
        }
        /// <summary>
        /// Creates a packet containing values
        /// enetered by the user.
        /// </summary>
        /// <param name="user">Username</param>
        /// <param name="pass">Password</param>
        /// <returns>An encrypted packet to send to server.</returns>
        public static byte[] CreateLoginPacket(string user, string pass)
        {
            byte[] login = { };
            try
            {
                // Login packets are (server id) 12
                string loginInfo = "12" + user + ":" + pass;
                byte[] loginBytes = Encoding.ASCII.GetBytes(loginInfo);
                login = EncDec.Encrypt(loginBytes, Parser.serverObject.EncryptionKey, Parser.serverObject.IVKey);
            }
            catch (Exception e)
            {
                Spanky.WriteLine(e.ToString(), Color.Red);
            }
            return login;
        }
    }
}
