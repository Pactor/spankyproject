﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpankyProjectClient.TCP;
using System.Threading;
using System.IO;

namespace SpankyProjectClient
{
    public partial class Spanky : Form
    {
        static Spanky Bot { get; set; }
        public static List<Thread> ThreadList { get; set; }
        Thread ClientThread { get; set; }
        // Key is the folder we pass without our dir stuff and value is the full path
        public static Dictionary<string, string> ProjectFiles { get; set; }
        public Spanky()
        {
            Bot = this;
            ThreadList = new List<Thread>();
            ProjectFiles = new Dictionary<string, string>();
            ClientThread = new Thread(Connect);
            ClientThread.IsBackground = true;
            InitializeComponent();
        }
        /// <summary>
        /// Write a message to our rtfoutput
        /// </summary>
        /// <param name="msg">the message</param>
        /// <param name="color">the color to write in</param>
        public static void Write(string msg, Color color)
        {

            if (Bot.rtfOutput.InvokeRequired)
            {
                Bot.rtfOutput.Invoke((MethodInvoker)delegate
                {
                    Bot.rtfOutput.SelectionColor = color;
                    Bot.rtfOutput.AppendText(msg);
                });
            }
            else
            {
                // Do not need an invoke, so just do it.
                Bot.rtfOutput.SelectionColor = color;
                Bot.rtfOutput.AppendText(msg);
            }
        }
        /// <summary>
        /// Write a message to our rtfoutput
        /// </summary>
        /// <param name="msg">the message</param>
        /// <param name="color">the color to write in</param>
        public static void WriteLine(string msg, Color color)
        {

            if (Bot.rtfOutput.InvokeRequired)
            {
                Bot.rtfOutput.Invoke((MethodInvoker)delegate
                {
                    Bot.rtfOutput.SelectionColor = color;
                    Bot.rtfOutput.AppendText(msg + "\n");
                });
            }
            else
            {
                // Do not need an invoke, so just do it.
                Bot.rtfOutput.SelectionColor = color;
                Bot.rtfOutput.AppendText(msg + "\n");
            }
        }


        private void Connect()
        {
            //AsynchronousClient.StartClient();
            // If we got here, we have svr and port values
            //spankyClient.Connect(txt_svrBox.Text, int.Parse(txt_portBox.Text));
            SpankyConnection c = new SpankyConnection();
            c.Connect(txt_svrBox.Text, int.Parse(txt_portBox.Text));
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txt_svrBox.Text.Length == 0) 
            {
                Spanky.Write("ERROR: No server to connect to.\n", Color.Red);
                return; 
            }
            if (txt_portBox.Text.Length == 0)
            {
                Spanky.Write("ERROR: No port to connect to.", Color.Red);
                return;
            }
            ClientThread.Start();
            Spanky.ThreadList.Add(ClientThread);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void checkSumAFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //PacketIO.ByteTest();
            //DialogResult result = openFileCheckSum.ShowDialog();

            //if (result == DialogResult.OK)
            //{
              //  Spanky.Write(PacketIO.GetChecksum(openFileCheckSum.FileName) + "\n", Color.Violet);
            //}
            //else { return; }
            
        }

        private void openProjectFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                openProjectFolder.Description = "Open Project Folder.";
                DialogResult result = openProjectFolder.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    // Whats the foldername ?
                    string folderName = openProjectFolder.SelectedPath.Split('\\').Last();

                    foreach (string file in Directory.GetFiles(openProjectFolder.SelectedPath))
                    {
                        // Strip all before the folder name
                        int fstart = file.IndexOf(folderName);
                        string folder = file.Remove(0, fstart);
                        //Spanky.Write(folder + "\n", Color.Magenta);
                        ProjectFiles.Add(folder, file);

                    }
                    // Build a Packet from first file pls
                    foreach (KeyValuePair<string, string> all in ProjectFiles)
                    {
                        /*
                        byte[] t2 = PacketIO.BuildInfoPacket(all.Key, all.Value);
                        StringBuilder l = new StringBuilder();
                        foreach (byte b in t2)
                        {
                            l.Append(b + " ");
                        }
                        Spanky.Write("In Bytes \n" + l.ToString() + "\n", Color.Magenta);
                        // Turn this packet back into a string
                        string test = Encoding.ASCII.GetString(PacketIO.BuildInfoPacket(all.Key,all.Value));
                        Spanky.Write("As a string \n" + test + "\n", Color.Black);
                        l.Clear();
                         */
                    }
                }
                else
                    return;
            }
            catch (Exception ee)
            {
                Spanky.Write(ee.ToString(), Color.Red);
            }
        }

        
    }
}
