﻿namespace SpankyProjectClient
{
    partial class Spanky
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Spanky));
            this.rtfOutput = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txt_svrBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_portBox = new System.Windows.Forms.TextBox();
            this.mainStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkSumAFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openProjectFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileCheckSum = new System.Windows.Forms.OpenFileDialog();
            this.openProjectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.mainStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtfOutput
            // 
            this.rtfOutput.Dock = System.Windows.Forms.DockStyle.Left;
            this.rtfOutput.HideSelection = false;
            this.rtfOutput.Location = new System.Drawing.Point(0, 24);
            this.rtfOutput.Name = "rtfOutput";
            this.rtfOutput.ReadOnly = true;
            this.rtfOutput.Size = new System.Drawing.Size(246, 274);
            this.rtfOutput.TabIndex = 0;
            this.rtfOutput.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(677, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Spanky Server Address";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(699, 142);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txt_svrBox
            // 
            this.txt_svrBox.Location = new System.Drawing.Point(680, 53);
            this.txt_svrBox.Name = "txt_svrBox";
            this.txt_svrBox.Size = new System.Drawing.Size(115, 20);
            this.txt_svrBox.TabIndex = 3;
            this.txt_svrBox.Text = "127.0.0.1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(677, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Spanky Server Port";
            // 
            // txt_portBox
            // 
            this.txt_portBox.Location = new System.Drawing.Point(680, 104);
            this.txt_portBox.Name = "txt_portBox";
            this.txt_portBox.Size = new System.Drawing.Size(115, 20);
            this.txt_portBox.TabIndex = 5;
            this.txt_portBox.Text = "1235";
            // 
            // mainStrip
            // 
            this.mainStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mainStrip.Location = new System.Drawing.Point(0, 0);
            this.mainStrip.Name = "mainStrip";
            this.mainStrip.Size = new System.Drawing.Size(807, 24);
            this.mainStrip.TabIndex = 7;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkSumAFileToolStripMenuItem,
            this.openProjectFolderToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // checkSumAFileToolStripMenuItem
            // 
            this.checkSumAFileToolStripMenuItem.Name = "checkSumAFileToolStripMenuItem";
            this.checkSumAFileToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.checkSumAFileToolStripMenuItem.Text = "CheckSum a File";
            this.checkSumAFileToolStripMenuItem.Click += new System.EventHandler(this.checkSumAFileToolStripMenuItem_Click);
            // 
            // openProjectFolderToolStripMenuItem
            // 
            this.openProjectFolderToolStripMenuItem.Name = "openProjectFolderToolStripMenuItem";
            this.openProjectFolderToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.openProjectFolderToolStripMenuItem.Text = "Open project Folder";
            this.openProjectFolderToolStripMenuItem.Click += new System.EventHandler(this.openProjectFolderToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openFileCheckSum
            // 
            this.openFileCheckSum.FileName = "openFileDialog1";
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(252, 53);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(415, 96);
            this.listView1.TabIndex = 8;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // listView2
            // 
            this.listView2.Location = new System.Drawing.Point(252, 178);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(415, 96);
            this.listView2.TabIndex = 9;
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // Spanky
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 298);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.txt_portBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_svrBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtfOutput);
            this.Controls.Add(this.mainStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainStrip;
            this.Name = "Spanky";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spanky";
            this.mainStrip.ResumeLayout(false);
            this.mainStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txt_svrBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_portBox;
        protected System.Windows.Forms.RichTextBox rtfOutput;
        private System.Windows.Forms.MenuStrip mainStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem checkSumAFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileCheckSum;
        private System.Windows.Forms.ToolStripMenuItem openProjectFolderToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog openProjectFolder;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
    }
}

