﻿#region License
/*
 * Microsoft Public License (Ms-PL)
 * 
 * This license governs use of the accompanying software. If you use the software, you accept this license. 
 * If you do not accept the license, do not use the software.
 *
 * 1. Definitions
 *
 * The terms "reproduce," "reproduction," "derivative works," and "distribution" have the same meaning here as under U.S. copyright law.
 *
 * A "contribution" is the original software, or any additions or changes to the software.
 *
 * A "contributor" is any person that distributes its contribution under this license.
 *
 * "Licensed patents" are a contributor's patent claims that read directly on its contribution.
 *
 * 2. Grant of Rights
 *
 * (A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, 
 * prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
 *
 * (B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, 
 * use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the 
 * contribution in the software.
 *
 * 3. Conditions and Limitations
 *
 * (A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
 *
 * (B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, 
 * your patent license from such contributor to the software ends automatically.
 *
 * (C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, 
 * and attribution notices that are present in the software.
 *
 * (D) If you distribute any portion of the software in source code form, you may do so only under this license by including 
 * a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, 
 * you may only do so under a license that complies with this license.
 *
 * (E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. 
 * You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, 
 * the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement. 
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace SpankyServer.Utilities
{
    public static class XMLManager
    {
        public static ServerConfiguration ServerConfig { get; set; }
        /// <summary>
        /// Load our config file.
        /// </summary>
        static XmlDocument Config
        {
            get
            {
                var c = new XmlDocument();
                c.Load("SpankyServerConfig.xml");
                return c;
            }
        }
        public static ServerConfiguration LoadConfig()
        {
            ServerConfiguration s = new ServerConfiguration();
            XmlNodeList list = Config.SelectNodes("/configuration/config");
            List<XmlNode> nodes = new List<XmlNode>();
            try
            {

                foreach (XmlNode n in list)
                {
                    // Load values in 
                    if (n["useMySql"].InnerText == "yes") { s.UseMySql = true; } else { s.UseMySql = false; }
                    if (n["useXML"].InnerText == "yes") { s.UseXML = true; } else { s.UseXML = false; }
                    if (n["useEmbeddedHTTP"].InnerText == "yes") { s.UseEmbeddedHTTPD = true; } else { s.UseEmbeddedHTTPD = false; }
                    if (n["useExternalHTTP"].InnerText == "yes") { s.UseExternalHTTP = true; } else { s.UseExternalHTTP = false; }
                    s.SpankyIP = n["spankyServerIP"].InnerText;
                    s.SpankyPort = int.Parse(n["spankyServerPort"].InnerText);
                    // Load values according to the config options
                    if (s.UseMySql)
                    {
                        XmlNode sql = Config.SelectSingleNode("/configuration/config/mysql");
                        // Load the sql options
                        s.MySqlServer = new Tuple<string, int, string>(
                            sql["servername"].InnerText, int.Parse(sql["serverPort"].InnerText), sql["database"].InnerText);
                    }

                    if (s.UseExternalHTTP)
                    {
                        // Load External Options
                    }
                    //Load Services
                    XmlNode svces = Config.SelectSingleNode("/configuration/config/services");
                    foreach (XmlNode svc in svces)
                    {
                        // Starts with service
                        bool enabled = false;
                        if (svc["enabled"].InnerText == "yes") { enabled = true; }
                        s.Services = new List<Tuple<string, string, string, bool>>();
                        s.Services.Add(new Tuple<string, string, string, bool>(
                            svc["serviceName"].InnerText, svc["serviceIp"].InnerText,
                            svc["servicePort"].InnerText, enabled));
                    }
                }
                // Attach the newly created server config
                XMLManager.ServerConfig = s;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return s;
        }
    }
}
