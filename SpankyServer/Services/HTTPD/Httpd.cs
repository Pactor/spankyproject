﻿#region License
/*
 * Microsoft Public License (Ms-PL)
 * 
 * This license governs use of the accompanying software. If you use the software, you accept this license. 
 * If you do not accept the license, do not use the software.
 *
 * 1. Definitions
 *
 * The terms "reproduce," "reproduction," "derivative works," and "distribution" have the same meaning here as under U.S. copyright law.
 *
 * A "contribution" is the original software, or any additions or changes to the software.
 *
 * A "contributor" is any person that distributes its contribution under this license.
 *
 * "Licensed patents" are a contributor's patent claims that read directly on its contribution.
 *
 * 2. Grant of Rights
 *
 * (A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, 
 * prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
 *
 * (B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, 
 * use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the 
 * contribution in the software.
 *
 * 3. Conditions and Limitations
 *
 * (A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
 *
 * (B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, 
 * your patent license from such contributor to the software ends automatically.
 *
 * (C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, 
 * and attribution notices that are present in the software.
 *
 * (D) If you distribute any portion of the software in source code form, you may do so only under this license by including 
 * a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, 
 * you may only do so under a license that complies with this license.
 *
 * (E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. 
 * You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, 
 * the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement. 
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO;
using SpankyServer.TCP;
using SpankyServer.Utilities;

namespace SpankyServer.Services
{
    public class Httpd : ISpankyService
    {
        List<Thread> ThreadList = new List<Thread>();
        SpankyServer.TCP.Server _Server { get; set; }
        private TcpListener Tcp { get; set; }
        /// <summary>
        /// Grabs the hostaddress from our xml config file.
        /// </summary>
        public string HostAddress
        {
            get
            {
                string reply = "";
                XMLManager.LoadConfig();
                // Grab the httpd server
                foreach (Tuple<string, string, string, bool> svc in XMLManager.ServerConfig.Services)
                {
                    if (svc.Item1 == "httpd")
                    {
                        reply = svc.Item2;
                    }
                }
                return reply;
            }
        }
        public int HostPort
        {
            get
            {
                int reply = 0;
                XMLManager.LoadConfig();
                // Grab the httpd server
                foreach (Tuple<string, string, string, bool> svc in XMLManager.ServerConfig.Services)
                {
                    if (svc.Item1 == "httpd")
                    {
                        reply = int.Parse(svc.Item3);
                    }
                }
                return reply;
            }
        }
        
        public void Initialize(SpankyServer.TCP.Server srv)
        {
            if (HostAddress.Length == 0 || HostPort <= 0)
            {
                Console.WriteLine("Configuration Error, Unable to start service HTTPD.");
                return;
            }
            Console.WriteLine("Starting Http Service on IP " + HostAddress + " Port " + HostPort);
            _Server = srv;
            // Create and run our Timer
            Thread Timer = new Thread(StartTimer);
            Timer.IsBackground = true;
            Timer.Name = "UpTimer";
            Timer.Start();
            ThreadList.Add(Timer);
            // Now run my service on another thread.
            Thread Service = new Thread(RunService);
            Service.IsBackground = true;
            Service.Name = "Httpd Service";
            Service.Start();
            ThreadList.Add(Service);
            Status = SpankyServiceStatus.Green;

        }
        public string ServiceName { get { return "Httpd"; } }
        public string UpTime
        {
            get
            {
                return ServiceName + " module uptime " + ServiceUpTime.Elapsed.Days + " Day(s) " +
                    ServiceUpTime.Elapsed.Hours + " Hour(s) " + ServiceUpTime.Elapsed.Minutes + " Minute(s) and " +
                    ServiceUpTime.Elapsed.Seconds + " Second(s).";
            }
        }
        
        public Stopwatch ServiceUpTime { get; set; }
        public void Dispose() { }
        void StartTimer()
        {
            ServiceUpTime = new Stopwatch();
            ServiceUpTime.Start();
        }
        public SpankyServiceStatus Status { get; set; }
        void RunService()
        {
            IPAddress Ip = null;

            // Try to parse ourip
            try { Ip = IPAddress.Parse(HostAddress); }
            catch 
            {
                try
                {
                    Ip = Dns.GetHostEntry(HostAddress).AddressList[0];
                }
                catch
                {
                    Console.WriteLine("Unable to parse " + HostAddress + " into an usable IPAddres.");
                    return;
                }
            }
            try
            {
                Tcp = new TcpListener(Ip, HostPort);
                Tcp.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            // Ok Server is started.

            while (Status == SpankyServiceStatus.Green || Status == SpankyServiceStatus.Yellow)
                if (Tcp.Pending())
                {
                    var sock = Tcp.AcceptSocket();


                    string msg = GetMsg(sock);
                    //string last_msg = null;//Used for post. POST doesn't end with double newlines, so this is used to check.
                    if (msg.StartsWith("GET"))
                    {
                        #region GET_Message_Receiving
                        while (!msg.EndsWith("\r\n\r\n"))
                        {
                            string val = GetMsg(sock);
                            if (val != "")
                            {
                                msg += val;
                            }
                            else
                            {
                                break;
                            }
                            if (msg == "")
                            {
                                break;
                            }
                        }
                        #endregion
                    }
                    else if (msg.StartsWith("POST"))
                    {
                        #region POST_Message_Receiving
                        while (!msg.Contains("\r\n\r\n") && !msg.EndsWith("\r\n\r\n"))
                        {
                            string val = GetMsg(sock);
                            if (val != "")
                            {
                                msg += val;
                            }
                            else
                            {
                                break;
                            }
                            if (msg == "")
                            {
                                break;
                            }
                        }
                        #endregion
                    }

                    if (msg != "")
                    {

                        //parse the data.
                        string[] d = msg.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        string method = d[0];
                        string[] method_ar = method.Split(new string[] { " " }, StringSplitOptions.None);
                        string method_type = method_ar[0]; //the HTTP Method. e.g. GET,POST, etc
                        string file = method_ar[1]; //The file being requested.
                        string http = method_ar[2]; //The http version. e.g. HTTP/1.1.
                        //bool _deflate = false; //Determines if to use deflate for HTTP compression.
                        string query = null;
                        try
                        {
                            //Gets any GET variables from the url.
                            query = file.Substring(file.IndexOf("?"));
                            file = file.Substring(0, file.IndexOf("?"));
                        }
                        catch (Exception)
                        {
                        }

                        switch (method_type)
                        {
                            case "GET":
                                SendGetResponse(sock, file, true, query);
                                break;
                            case "HEAD":
                                SendGetResponse(sock, file, false); //Process the request. Sends only the headers and not the actual file.
                                break;
                            case "POST":
                                string[] postdata = d[d.Length - 1].Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries); //Get the POSTed data and begin to send a response back to the client.
                                //PHP_Test_SendPostResponse(tc.Client, file, postdata);
                                break;
                        }
                    }
                }
                else
                    System.Threading.Thread.Sleep(50);

        }
        private static byte[] GetData(string query, out bool found)
        {
            byte[] msg = { };
            // File Structure
            // Dir first
            bool hasReturn = false;
            if (!Directory.Exists("Services//HTTPD//htdocs")) { Directory.CreateDirectory("Services//HTTPD//htdocs"); }
            List<string> rootFiles = new List<string>(Directory.GetFiles("Services//HTTPD//htdocs"));
            // get this service in our list.
            ISpankyService service = null;
            foreach(ISpankyService s in Parser.Services)
            {
                if(s.ServiceName == "Httpd"){ service = s; }
            }
            foreach (string f in rootFiles)
            {
                // Index.html first if not there look for .htm
                if (f.Split('\\').Last() == "index.html")
                {
                    // We have a index.html use this first.
                    string text = System.IO.File.ReadAllText(f);
                    if (text.Contains("|HTTPDUPTIME|"))
                    {
                        string split1 = text.Split('|').First();
                        //Console.WriteLine("Split1 " + split1);
                        string split2 = text.Split('|').Last();
                        //Console.WriteLine("Split2 " + split2);
                        string uptime = split1 + service.UpTime + split2;
                        msg = Encoding.ASCII.GetBytes(uptime);
                    }
                    else
                    msg = Encoding.ASCII.GetBytes(text);
                    hasReturn = true;
                }
                else if (f.Split('\\').Last() == "index.htm")
                {
                    // We have a index.html use this first.
                    string text = System.IO.File.ReadAllText(f);

                    msg = Encoding.ASCII.GetBytes(text);
                    hasReturn = true;
                }
            }
            /*
            var header = "<html><head><title>" + (query == null ? "Home" : query) + " - Spanky</title></head>";

            string body = "<body>";

            switch (query)
            {
                case null:
                    {
                        found = true;

                        body += "<p>This is the home page of Spanky's web interface.</p>\r\n<a href=\"?status\">Status</a>";
                        break;
                    }
                case "?status":
                    {
                        found = true;
                        foreach (ISpankyService srv in Parser.Services)
                        {
                            body += "<p>" + srv.ServiceName + " - " + Enum.GetName(typeof(SpankyServiceStatus), srv.Status) + "</p>\r\n";
                            body += "<p>" + srv.UpTime + "</p>\r\n";
                        }
                        break;
                    }
                default: found = false;
                    break;
            }

            body += "</body>";

            return new System.Text.ASCIIEncoding().GetBytes(header + body + "</html>");
             */
            if (hasReturn) { found = true; }
            else { found = false; }
            return msg;
        }

        #region Code from Waterbird, my other HTTPd project
        private static void SendGetResponse(Socket s, string file, bool sendfile, string query = null)
        {
            //Sets up a stringbuilder and gets ready to make a response.
            StringBuilder sb = new StringBuilder();
            bool found = false;
            byte[] data = GetData(query, out found);

            if (file == "/" && found) //Since its requesting the index file, I'll make a special request to the index.htm.
            {
                sb.AppendLine("HTTP/1.1 200 OK"); //Sends the OK back to the browser.
                sb.AppendLine("Date: " + DateTime.Today.ToLongDateString()); //The current time.
                sb.AppendLine("Server: Sayuka HTTPd (based on Waterbird)");     //Lets the browser know "my" name.    
                sb.AppendLine("Content-Encoding: none"); //Tells the browser that the data is deflated.

                //sb.AppendLine("Accept-Ranges: bytes");
                sb.AppendLine("Connection: close");//Tells the browser that the connection will close.
                sb.AppendLine(); //This seperates the above headers and the page content.
                s.Send(new System.Text.ASCIIEncoding().GetBytes(sb.ToString())); //Sends the headers.
                if (sendfile == true) //Checks if its a GET or a HEAD request. if its a GET, it sends the file using the code below.
                {
                    s.Send(data); //Sends the requested file.
                }
            }
            else
            {
                sb.AppendLine("HTTP/1.1 404 Not Found");
                sb.AppendLine("Date: " + DateTime.Today.ToLongDateString()); //The current time.
                sb.AppendLine("Server: Sayuka HTTPd (based on Waterbird)");     //Lets the browser know "my" name.    
                sb.AppendLine("Content-Encoding: none"); //Tells the browser that the data is deflated.
                sb.AppendLine("Connection: close");

                s.Send(new System.Text.ASCIIEncoding().GetBytes(sb.ToString()));
            }

            s.Close(); //Finally, closes the socket.
            //s.Shutdown(SocketShutdown.Both);
        }
        private static byte[] CompressData_Deflate(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            System.IO.Compression.DeflateStream ds = new System.IO.Compression.DeflateStream(ms, System.IO.Compression.CompressionMode.Compress);
            ds.Write(data, 0, data.Length);
            ds.Close();
            byte[] compressed = (byte[])ms.ToArray();
            ms.Close();
            return compressed;
        }
        private static byte[] CompressData_GZip(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            System.IO.Compression.GZipStream ds = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress);
            ds.Write(data, 0, data.Length);
            ds.Close();
            byte[] compressed = (byte[])ms.ToArray();
            ms.Close();
            return compressed;
        }
        private static string GetMsg(Socket s)
        {
            //This is used for converting bytes to strings.
            byte[] b = new byte[100];
            int k = s.Receive(b);
            StringBuilder msg = new StringBuilder();
            for (int i = 0; i < k; i++)
                msg.Append(Convert.ToChar(b[i]));
            return msg.ToString();
        }
        #endregion

        public void Stop()
        {
            this.Status = SpankyServiceStatus.Red;
            foreach (Thread t in ThreadList)
            {
                t.Abort();
            }
        }
    }
}
