﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace SpankyServer
{
    class Program
    {
        public static Thread cmds;
        static ConsoleCommands consoleCmds = new ConsoleCommands();

        static void Main(string[] args)
        {
            cmds = new Thread(consoleCmds.WaitForCommands);
            //cmds.IsBackground = true;
            cmds.Name = "Command Thread.";
            consoleCmds.ThreadList.Add(cmds);
            // Start it
            cmds.Start();
        }
    }
}
