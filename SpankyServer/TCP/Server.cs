﻿#region License
/*
 * Microsoft Public License (Ms-PL)
 * 
 * This license governs use of the accompanying software. If you use the software, you accept this license. 
 * If you do not accept the license, do not use the software.
 *
 * 1. Definitions
 *
 * The terms "reproduce," "reproduction," "derivative works," and "distribution" have the same meaning here as under U.S. copyright law.
 *
 * A "contribution" is the original software, or any additions or changes to the software.
 *
 * A "contributor" is any person that distributes its contribution under this license.
 *
 * "Licensed patents" are a contributor's patent claims that read directly on its contribution.
 *
 * 2. Grant of Rights
 *
 * (A) Copyright Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free copyright license to reproduce its contribution, 
 * prepare derivative works of its contribution, and distribute its contribution or any derivative works that you create.
 *
 * (B) Patent Grant- Subject to the terms of this license, including the license conditions and limitations in section 3, 
 * each contributor grants you a non-exclusive, worldwide, royalty-free license under its licensed patents to make, have made, 
 * use, sell, offer for sale, import, and/or otherwise dispose of its contribution in the software or derivative works of the 
 * contribution in the software.
 *
 * 3. Conditions and Limitations
 *
 * (A) No Trademark License- This license does not grant you rights to use any contributors' name, logo, or trademarks.
 *
 * (B) If you bring a patent claim against any contributor over patents that you claim are infringed by the software, 
 * your patent license from such contributor to the software ends automatically.
 *
 * (C) If you distribute any portion of the software, you must retain all copyright, patent, trademark, 
 * and attribution notices that are present in the software.
 *
 * (D) If you distribute any portion of the software in source code form, you may do so only under this license by including 
 * a complete copy of this license with your distribution. If you distribute any portion of the software in compiled or object code form, 
 * you may only do so under a license that complies with this license.
 *
 * (E) The software is licensed "as-is." You bear the risk of using it. The contributors give no express warranties, guarantees or conditions. 
 * You may have additional consumer rights under your local laws which this license cannot change. To the extent permitted under your local laws, 
 * the contributors exclude the implied warranties of merchantability, fitness for a particular purpose and non-infringement. 
 */
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cell.Core;
using SpankyServer.Services;
using SpankyServer.Utilities;

namespace SpankyServer.TCP
{
    public class Server : ServerBase
    {
        #region override
        public Server()
            : base()
        {
            // Load our config and servers
            LoadConfig();
        }

        protected override void OnSendTo(System.Net.IPEndPoint clientIP, int num_bytes)
        {
        }
        protected override void OnReceiveUDP(int num_bytes, byte[] buf, System.Net.IPEndPoint ip)
        {
        }
        protected override ClientBase CreateClient()
        {
            Console.WriteLine("SpankyServer::CreateClient");
            return new Client(this);
        }
        protected override bool OnClientConnected(ClientBase client)
        {
            Console.WriteLine("SpankyServer::OnClientConnected");
            byte[] hello = Parser.BuildPacket((Client)client, 10);

            client.Send(ref hello);
            Console.WriteLine("Sent Hello.");
            return base.OnClientConnected(client);
        }
        protected override void OnClientDisconnected(ClientBase client, bool forced)
        {
            Console.WriteLine("SpankyServer::OnClientDisconnected");
            base.OnClientDisconnected(client, forced);
        }
        public override void Start()
        {
            Console.WriteLine("SpankyServer::Start");
            base.Start();
        }
        public override void Stop()
        {
            Console.WriteLine("SpankyServer::Stop");
            base.Stop();
        }
        #endregion

        #region Custom
        public void LoadConfig()
        {
            XMLManager.LoadConfig();
            ServerConfiguration s = XMLManager.ServerConfig;
            if (s.UseEmbeddedHTTPD == true)
            {
                Httpd serv = new Httpd();
                //serv.Initialize(this);
                Parser.Services.Add(serv);
            }
        }
        #endregion
    }
}
